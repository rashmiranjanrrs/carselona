const mysql = require("mysql")

class DB{
    makeConnection(){
        return mysql.createConnection({
            host: "localhost",
            user: "root",
            password: "",
            database: "ranjan"
        })
    }
}

module.exports = new DB();