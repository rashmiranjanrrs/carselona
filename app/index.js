const express = require("express")
const app = express()
const path= require("path");
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

const userRouter = require("../routers/UserRouter")
const homeRouter = require("../routers/HomeRouter")

app.set('view engine', 'ejs');

app.use(express.static(path.resolve(__dirname,'assets')));

app.use("/users", userRouter)
app.use("/", homeRouter)
app.listen(8000)