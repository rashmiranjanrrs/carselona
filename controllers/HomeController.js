class HomeController{
    getHome(req, res){ 
        res.render("home")
    }

    getClockView(req, res){ 
        var data = {};
        var ans = 0;
        if(req.query.time){
            data = req.query.time
            var time = data.split(':');
            
            time[0] = parseInt(time[0])
            time[1] = parseInt(time[1])

            var mins = (time[0] * 60) + time[1]

            var hrHandAngle =  mins * 1/2;
            var minHand = time[1] * 6;
            ans = hrHandAngle - minHand
            console.log(ans);
        }
        res.render("questions/clock",{time: data, ans: Math.abs(ans)})
    }

    getFireView(req, res){ 
        var data = req.query;
        var answerKey = '';
        var result = 0;
        if(req.query){
            var resultHill = getResult(data.speed_hill, data.distance_hill);
            var resultRoad = getResult(data.speed_road, data.distance_road);
            var resultWind = getResult(data.speed_wind, data.distance_wind);
            var data_ = {
                hill: resultHill,
                road: resultRoad,
                wind: resultWind
            }
            result = Math.min(resultHill,resultRoad,resultWind)
            answerKey = getKeyByValue(data_, result)
        }
        res.render("questions/fire",{answerKey: answerKey, result: result})


        function getResult(speed, distance){
            var timeTaken = (60 / speed) * distance
            return timeTaken > 60 ? timeTaken/60 : timeTaken
        }
    
        function getKeyByValue(object, value) {
            return Object.keys(object).find(key => object[key] === value);
        }
    }

    getVegitablesView(req, res){
        var ans = 0
        if(req.query){
            var amount = req.query.amount
            ans = amount * 2 /5
        }
        res.render("questions/vegitables", {ans: ans})
    }

    getPianoView(req, res){
        res.render("questions/piano")
    }

    getStoreView(req, res){
        res.render("questions/store")
    }

    getBagView(req, res){
        res.render("questions/bag")
    }

    getugbyView(req, res){
        var ans = 0
        if(req.query){
            var total_teams = req.query.total_teams
            ans = total_teams - 1
        }
        res.render("questions/rugby",{ans: ans})
    }
}

module.exports = new HomeController()