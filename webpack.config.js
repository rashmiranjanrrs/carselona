const path = require('path');

module.exports = {
    entry: ['./app'],
    output: {
        path: __dirname + '/build',
        filename: 'bundle.js'
    },
    resolve: {
        // alias: {
        //     '@': path.resolve('resources/js'),
        // },
        fallback: {
            crypto: require.resolve('crypto-browserify'),
            stream: require.resolve('stream'),
            buffer: require.resolve("buffer") ,
            timers: require.resolve("timers-browserify"),
            url: require.resolve("url"),
            http: require.resolve("stream-http"),
            fs: false,
            tls: false,
            zlib: false
        },
    },
};